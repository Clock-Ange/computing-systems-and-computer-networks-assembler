.386
 .model flat, stdcall
 option casemap :none
 include \masm32\include\windows.inc
 include \masm32\include\user32.inc
 include \masm32\include\kernel32.inc
 includelib \masm32\lib\user32.lib
 includelib \masm32\lib\kernel32.lib
 .data
    Array           dd 3,5,6,7,9,10 ;����������� ������
    ArrayLength=$-Array
    OnesCounter     db 0
    BArray          dd ArrayLength dup(?)
    BAdressArray    dd ArrayLength dup(?)
    shiftCounter    db 0
    msg1            db "=%d",0
    sResult         byte 50 dup (?)
    zResult         db "there isn't such elements"
    Zagolovok       db "result",0
    sfc             db "There %.1d",10," element with two ones", 0
    result          db 0
 .code
 start: 
        XOR     EBX, EBX
        XOR     ESI, ESI

                
        ArrayLoop:
                cmp     ebx, ArrayLength
                jge     endLoop
                mov     eax,Array[EBX]   ; �����
                mov     eax,[EBX]
        CountLoop:
                test    eax, 1           ; ��������� ��������� ��� �����
                jnz     add_ones        ; ����� 1-�� - ����� �������� - ��������� � ���������� ����� ������
                jmp     next            ; ��������� � ��������� ���������� ����
        add_ones:
                inc     OnesCounter               

        next:
                shr     eax, 1           ; �������� AX ������ �� 1 ���
                inc     shiftCounter
                mov     ecx, eax          ; � ������� ����� ���������� AX � CX
                jcxz    nextElem        ; ���� � AX ��� ���� - �������� ��� ��� ��� - ���� � ���������� ��������                
                jmp     CountLoop       ; �� ���� - ��� �������� ���-�� ���������
        nextElem:
                cmp     OnesCounter, 2
                mov     OnesCounter, 0
                JZ      addToBArray
                JMP     STOP
        addToBArray:
                mov     edx,Array[EBX]
                MOV     BArray[EBX], edx
                MOV     BAdressArray[EBX], EBX
                inc     result
                ADD     ESI, 4
                JMP     STOP
        STOP:
                ADD     EBX,4
        LOOP ArrayLoop
        
        endLoop:
                cmp     result, 0
                JZ      ZeroOutput
        output:       
                invoke wsprintfA, ADDR sResult, ADDR sfc, result 
                invoke MessageBox, NULL, ADDR sResult, ADDR Zagolovok, MB_OK
                invoke ExitProcess,NULL
        ZeroOutput:
                invoke MessageBox, NULL, ADDR zResult, ADDR Zagolovok, MB_OK
 end start ;����� ���������