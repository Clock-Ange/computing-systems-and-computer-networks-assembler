//
// Created by Администратор on 15.09.2020.
//

#include "MeteoStation.h"

#include <iostream>

using namespace std;

bool abovezero(float num) {
    if (num >= 0) {
        return true;
    } else {

        throw invalid_argument("atm pressure and wind velocity cant'b be less, then 0");

    };
};

//по умолчанию
MeteoStation::MeteoStation() {
    temp = 273.15;
    atm = 100000;
    w_dir_N = 0;
    w_dir_S = 0;
    w_vel = 0;
};
//инициализирущий
MeteoStation::MeteoStation(float vtemp, float vatm, float vw_vel, float vw_dir_N, float vw_dir_S) {
    if (abovezero(vtemp) && abovezero(vatm) && abovezero(vw_vel)) {
        temp = vtemp;
        atm = vatm;
        w_vel = vw_vel;
        w_dir_N = vw_dir_N;
        w_dir_S = vw_dir_S;
        point=nullptr;

    }
};
//копирующий
MeteoStation::MeteoStation(const MeteoStation &obj) {
    temp = obj.temp;
    atm = obj.atm;
    w_vel = obj.w_vel;
    w_dir_N = obj.w_dir_N;
    w_dir_S = obj.w_dir_S;


};
//дестурктор
MeteoStation::~MeteoStation() = default;


const float &MeteoStation::getTemp() const {
    return point[1];
};

const float &MeteoStation::getAtm() const  {
    return atm;
};

const float &MeteoStation::getW_vel()  const {
    return w_vel;
};

const float &MeteoStation::getW_dir_N()  const {
    return w_dir_N;
};

const float &MeteoStation::getW_dir_S()  const {
    return w_dir_S;
};

void MeteoStation::setTemp( const float &vtemp) {
    temp = vtemp;
}

void MeteoStation::setAtm( const float &vatm) {
    atm = vatm;

}

void MeteoStation::setW_vel( const float &vw_vel) {
    w_vel = vw_vel;
}

void MeteoStation::setW_dir_N( const float &vw_dir_N) {
    w_dir_N = vw_dir_N;
}

void MeteoStation::setW_dir_S( const float &vw_dir_S) {
    w_dir_S = vw_dir_S;
}