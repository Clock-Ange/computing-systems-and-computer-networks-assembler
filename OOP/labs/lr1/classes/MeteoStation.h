//
// Created by Администратор on 15.09.2020.
//

#ifndef LR1_METEOSTATION_H
#define LR1_METEOSTATION_H

class MeteoStation {
private:
    float temp, atm, w_vel, w_dir_N, w_dir_S;
    float *point;
public:
    //по умолчанию
    MeteoStation();

    //инициализирующий
    MeteoStation(float vtemp,float vatm,float vw_vel,float vw_dir_N,float vw_dir_S );

    //копирующий
    MeteoStation(const MeteoStation &obj);

    ~MeteoStation();
    //геттеры
    const float &getTemp() const ;

    const float &getAtm() const ;

    const float &getW_vel() const ;

    const float &getW_dir_N() const ;

    const float &getW_dir_S() const ;
    //сеттеры
    void setTemp( const float &vtemp);

    void setAtm( const float &vatm);

    void setW_vel( const float &vw_vel);

    void setW_dir_N( const float &vw_dir_N);

    void setW_dir_S( const float &vw_dir_S);

};

#endif //LR1_METEOSTATION_H
