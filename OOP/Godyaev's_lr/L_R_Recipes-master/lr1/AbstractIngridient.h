#include <QString>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>
#ifndef ABSTRACTINGRIDIENT_H
#define ABSTRACTINGRIDIENT_H
class AbstractIngridient
{
public:
    AbstractIngridient()
    {
        name = "water";
    }
    AbstractIngridient(const QString tmp)
    {
        name = "water";
        set_name(tmp);
    }
    virtual ~AbstractIngridient(){}
    virtual bool get_obj_class() =0;
    QString get_name()
    {
        return name;
    }
    void set_name(const QString tmp)
    {
        if (tmp.size())
            name = tmp;
    }
    virtual void xml_file_write(QXmlStreamWriter&, int) =0;
protected:
    QString name;
};
#endif // ABSTRACTINGRIDIENT_H
