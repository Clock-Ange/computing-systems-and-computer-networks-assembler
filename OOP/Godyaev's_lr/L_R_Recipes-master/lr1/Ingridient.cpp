#include <QString>
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>
#include"Ingridient.h"
#ifndef Ingridient_cpp
#define Ingridient_cpp
Ingridient::Ingridient()
{
	quantity = 1;
	units_of_measure = l;
};
Ingridient::Ingridient(const QString tmp, const double tmp2, Units tmp3) : AbstractIngridient(tmp)
{
	quantity = 1;
    units_of_measure = l;
	set_quantity(tmp2);
	set_units(tmp3);
};
Ingridient::Ingridient(const Ingridient &ingridient) :AbstractIngridient(ingridient)
{
	units_of_measure = ingridient.units_of_measure;
	quantity = ingridient.quantity;
}
Ingridient::~Ingridient(){};
Units Ingridient::get_units_of_measure() const
{
	return units_of_measure;
}
double Ingridient::get_quantity() const
{
	return quantity;
}
void Ingridient::set_quantity(double const tmp)
{
	if (tmp <= 0)
    {
        quantity = 1;
        return;
    }
    quantity = round(tmp*100.0)/100.0;
}
void Ingridient::set_units(const Units  tmp)
{
    units_of_measure = tmp;
}
Ingridient &Ingridient::operator =(Ingridient& tmp)
{
    name = tmp.get_name();
    units_of_measure = tmp.get_units_of_measure();
    quantity = tmp.get_quantity();
    return *this;
}
Ingridient& Ingridient:: operator = (nullptr_t)
{
    name = "";
    quantity = 0;
    units_of_measure = l;
    return *this;
}
bool Ingridient ::  operator == (nullptr_t)
{
    if(name == "" &&
            quantity == 0 &&
            units_of_measure == l)
        return true;
    else
        return false;
}
bool Ingridient ::  operator != (nullptr_t)
{
    if(name == "" &&
            quantity == 0 &&
            units_of_measure == l)
        return false;
    else
        return true;
}
bool Ingridient :: operator != (Ingridient tmp)
{
    if (name != tmp.get_name() &&
            quantity != tmp.get_quantity() &&
            units_of_measure != tmp.get_units_of_measure())
        return true;
    else
        return  false;
}
bool Ingridient::get_obj_class()
{
    return true;
}
void Ingridient::xml_file_write(QXmlStreamWriter &xml_writer, int i)
{
    map<Units,QString> units_to_string;
    units_to_string.insert(pair<Units, QString>(kg, "kg"));
    units_to_string.insert(pair<Units, QString>(l, "l"));
    units_to_string.insert(pair<Units, QString>(units, "units"));
    xml_writer.writeStartElement("Ingridient");
    xml_writer.writeAttribute("number", QString :: number(i));
    xml_writer.writeStartElement("Name");
    xml_writer.writeCharacters(get_name());
    xml_writer.writeEndElement();

    xml_writer.writeStartElement("Units");
    xml_writer.writeCharacters
            (units_to_string[get_units_of_measure()]);
    xml_writer.writeEndElement();

    xml_writer.writeStartElement("Quantity");
    xml_writer.writeCharacters
            (QString :: number(get_quantity()));
    xml_writer.writeEndElement();
    xml_writer.writeEndElement();
}
Ingridient* Ingridient::xml_file_read(QXmlStreamReader &xml_reader)
{
    map<QString,Units> units_to_string;
    units_to_string.insert(pair<QString,Units>("kg", kg));
    units_to_string.insert(pair<QString,Units>("l", l));
    units_to_string.insert(pair<QString,Units>("units", units));
    Ingridient* current = new Ingridient;
    xml_reader.readNextStartElement();
    if (xml_reader.name() == "Name")
        current->set_name(xml_reader.readElementText());
    xml_reader.readNextStartElement();
    if (xml_reader.name() == "Units")
        current->set_units(units_to_string[xml_reader.readElementText()]);
    xml_reader.readNextStartElement();
    if (xml_reader.name() == "Quantity")
        current->set_quantity(xml_reader.readElementText().toDouble());
    xml_reader.readNextStartElement();
    return current;
}
#endif // !Ingridient_cpp

