#include "Iterator.h"
#include "action.h"
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>
#include <QFile>
#ifndef Recipe_H
#define Recipe_H

class Recipe
{
public:
    Recipe();
    Recipe(const Recipe&);
	~Recipe();
    void push(Ingridient*);
    void push(Action*);
    AbstractIngridient* pop();
    AbstractIngridient* peek() const;
    void erase();
    int get_elem_count() const;
    AbstractIngridient* get_item(int tmp) const;
    OwnIter begin();
    OwnIter end();
    void xml_file_write(const QString)const;
    void xml_file_read(const QString);
    bool operator != (nullptr_t);
    bool check_all() const;
    bool is_empty();
private:
    Node *head, *tail;
    int count;
};

#endif // !Recipe_H
