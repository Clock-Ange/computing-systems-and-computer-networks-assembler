#ifndef COUNTER_H
#define COUNTER_H
#include <QString>
#include <QCoreApplication>
#include <QCryptographicHash>
#include <QTextStream>
#include <QFile>
#include <cmath>
long long getHash(const QString s)
{
    QByteArray hash = QCryptographicHash::hash((s.toLocal8Bit()),
                                               QCryptographicHash::Md5);
    long long ii = 0;
    for (int i = 0; i < hash.size(); i++)
    {
        ii += static_cast<long long>(abs(hash[i]*pow(10,i)));
    }
    return ii;
}
long long getHash(const int s)
{
    QString stroke = QString::number(s);
    QByteArray hash = QCryptographicHash::hash((stroke.toLocal8Bit()),
                                               QCryptographicHash::Md5);
    long long ii = 0;
    for (int i = 0; i< hash.size(); i++)
    {
        ii += static_cast<long long>(abs(hash[i]*pow(10,i)));
    }
    return ii;
}
template <typename T>
struct elem
{
    int value;
    T key;
};

template <typename K>
class Counter
{
public:
    Counter()
    {
        size = 0;
        initCollection();
    }
    Counter(const Counter& tmp)
    {
        size = tmp.size - 1;
        initCollection();
        for (int i = 0; i < static_cast<int>(pow(2, tmp.size)); i++)
        {
            if (tmp.array[i])
            {
                elem<K> *current = new elem<K>();
                current->key = tmp.array[i]->key;
                current->value = tmp.array[i]->value;
                array[i] = current;
                //array[i]->key = tmp.array[i]->key;
                //array[i]->value = tmp.array[i]->value;
            }
        }
        numOfFullSpaces = tmp.numOfFullSpaces;
    }
    ~Counter()
    {
        for (int i = 0; i < static_cast<int>(pow(2, size)); i++)
        {
            delete array[i];
        }
        delete []  array;
    }
    void removeAll()
    {
        for (int i = 0; i < static_cast<int>(pow(2, size)); i++)
        {
            delete array[i];
        }
        size = 0;
        initCollection();
    }
    int getCollectionSize() const
    {
        return numOfFullSpaces;
    }
    bool operator ==(const Counter& tmp)
    {
        if (size == tmp.size)
        {
            for (int i = 0; i < static_cast<int>(pow(2, size)); i++)
            {
                if (tmp.array[i])
                {
                    if (searchCurrentKey(tmp.array[i]->key) == -1)
                        return false;
                }
            }
            return true;
        }
        else
            return false;
    }
    void operator <<(const K item)
    {
        if (isKeyExist(item) == false)
        {
            if (numOfFullSpaces >= static_cast<int>(pow(2, size)/2))
            {
                addMemory();
            }
            elem<K> *current  = new elem<K>();
            current->key = item;
            current->value = 1;
            int index = searchFreeSpace(item);
            array[index] = current;
            numOfFullSpaces++;
        }
        else
        {
            int index = searchCurrentKey(item);
            array[index]->value += 1;
        }
    }
    void deleteElem(const K item)
    {
        for (int i = getHash(item) % static_cast<int>(pow(2, size));
             i < static_cast<int>(pow(2, size)); i++)
        {
            if (array[i])
                if (array[i]->key == item)
                {
                    delete array[i];
                    array[i] = nullptr;
                    numOfFullSpaces--;
                    break;
                }
        }
    }
    int operator[] (const K item) const
    {
        for (int i = getHash(item) % static_cast<int>(pow(2, size));
             i < static_cast<int>(pow(2, size)); i++)
        {
            if (array[i])
                if (array[i]->key == item)
                    return array[i]->value;
        }
        return -1;
    }
    bool isKeyExist(const K item) const
    {
        for (int i = getHash(item) % static_cast<int>(pow(2, size));
             i < static_cast<int>(pow(2, size)); i++)
        {
            if (array[i])
                if (array[i]->key == item)
                {
                    return true;
                }
        }
        return false;
    }
    void writeTXTFile(const QString filename = "C:\\lr_2\\example1.txt") const
    {
        QFile writeFile(filename);
        if (size > 1)
        {
            writeFile.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream stream(&writeFile);
            for (int i = 0 ; i < static_cast<int>(pow(2, size)); i++)
            {
                if (array[i] != nullptr)
                {
                    stream << array[i]->key << " " <<
                              QString::number(array[i]->value) << "\n";
                }
            }
            writeFile.close();
        }
    }
    void readTXTFile(const QString filename)
    {
        removeAll();
        initCollection();
        QFile readFile(filename);
        if (readFile.exists())
        {
            readFile.open(QIODevice::ReadOnly | QIODevice::Text);
            QStringList str;
            QTextStream stream(&readFile);

            while (!readFile.atEnd())
            {
                str  << readFile.readLine();

            }
            numOfFullSpaces = str.size();
            for (int i = 0; i < str.size(); i++)
            {

                QStringList oneItem = str[i].split(" ");
                if (isKeyExist(oneItem[0]) == false)
                {
                    while(numOfFullSpaces >= static_cast<int>(pow(2,size)/2))
                    {
                        addMemory();
                    }
                    elem<K> *current = new elem<K>();
                    current->key = oneItem[0];
                    current->value = oneItem[1].toInt();
                    int index = searchFreeSpace(oneItem[0]);
                    array[index] = current;
                }
            }
            readFile.close();

        }
    }
private:
    int size;
    elem<K> **array;
    long numOfFullSpaces;
    void initCollection()
    {
        size++;
        array = new elem<K>*[static_cast<int>(pow(2, size))];
        for (int i = 0 ; i < static_cast<int>(pow(2, size)); i++)
        {
            array[i] = nullptr;
        }
        numOfFullSpaces = 0;
    }
    int searchFreeSpace(const K item) const
    {
        for(int j = getHash(item) % static_cast<int>(pow(2, size));
            j < static_cast<int>(pow(2, size)); j++)
        {
            if (!array[j])
                return j;
        }
        return -1;
    }
    int searchCurrentKey(const K item) const
    {
        for(int j = getHash(item) % static_cast<int>(pow(2, size));
            j < static_cast<int>(pow(2, size)); j++)
        {
            if (array[j])
                if (array[j]->key  == item)
                    return j;
        }
        return -1;
    }
    void addMemory()
    {
        elem<K> **array1;
        array1 = new elem<K>*[static_cast<int>(pow(2, size))];
        for (int i = 0 ; i < static_cast<int>(pow(2, size)); i++)
        {
            array1[i] = nullptr;
        }
        for (int i = 0; i < static_cast<int>(pow(2, size)); i++)
        {
            if (array[i])
                array1[i] = array[i];
        }
        int tmp = numOfFullSpaces;
        initCollection();
        numOfFullSpaces = tmp;
        for (int i = 0; i < static_cast<int>(pow(2, size-1)); i++)
        {
            if (array1[i])
            {
                int index = searchFreeSpace(array1[i]->key);
                elem<K> *current = new elem<K>();
                current->key = array1[i]->key;
                current->value = array1[i]->value;
                array[index] = array1[i];
            }
        }
    }
};

#endif // COUNTER_H
