#include "parking.h"
#include "car.h"
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
Parking::Parking()
{
    car_l =  new List<Car>();
    car_o_l = new List<CarOwner>();
}
Parking::Parking(const Parking& tmp) :
    car_o_l(tmp.car_o_l), car_l(tmp.car_l)
{
    number_of_free_spaces = tmp.number_of_free_spaces;
}
Parking::~Parking()
{
    car_o_l->remove_all();
    car_l->remove_all();
}
CarOwner* Parking::find_item( QString tmp) const
{
    if (car_l != nullptr && car_o_l != nullptr)
    {
        OwnIter<CarOwner> iter(car_o_l->front());
        QString q = tmp.left(tmp.indexOf(" "));
        tmp = tmp.remove(tmp.indexOf(q),q.length()+1);
        QString q1 = tmp.left(tmp.indexOf(" "));
        tmp = tmp.remove(tmp.indexOf(q1), q1.length()+1);
        for (int i = 1; i < car_o_l->get_size()+1; i++)
        {
            if (iter.get_current()->get_name() == q &&
                    iter.get_current()->get_time() == q1.toInt() &&
                    iter.get_current()->get_experience() == tmp.toInt())
            {
                CarOwner *to_return = iter.get_current();
                return to_return;
            }
            iter.next();
        }
    }
    std::exception();
}
void Parking::add_elem(CarOwner* tmp)
{
    if (number_of_free_spaces > 0)
    {
    car_o_l->insert_end(tmp);
    car_l->insert_end(tmp->get_vehicle());
    number_of_free_spaces--;
    }

}
void* Parking::delete_elem(const CarOwner* tmp)
{
    car_o_l->take(tmp);
    car_l->take(tmp->get_vehicle());
    number_of_free_spaces++;
}
Rate Parking::count_sum()
{
    Rate tariff;
    OwnIter<CarOwner> iter(car_o_l->front());
    for (int i = 1; i < car_o_l->get_size()+1; ++i)
    {
        double percentage = 100;
        if (iter.get_current()->get_experience() > 3)
            percentage -= 20;
        if (iter.get_current()->get_experience() > 5)
            percentage -= 10;
        switch (iter.get_current()->get_vehicle()->get_car_type())
        {
        case 1:
        {
            //Auto *tmp = dynamic_cast<Auto*>
            //   (iter.get_current()->get_vehicle());
        }
            break;
        case 2:
        {
            Truck *tmp = dynamic_cast<Truck*>
                    (iter.get_current()->get_vehicle());
            percentage += 10;
            if (tmp->get_tonnage() > 3.5)
                percentage += 15;
            if (tmp->get_tonnage() > 12.5)
                percentage += 15;
        }
            break;
        case 3:
        {
            SpecialTr *tmp = dynamic_cast<SpecialTr*>
                    (iter.get_current()->get_vehicle());
            switch (tmp->get_car_type())
            {
            case Motobyke:
            {
                percentage += 10;
            }
                break;
            case ATV:
            {
                percentage += 15;
            }
                break;
            case Scooter:
            {
                percentage += 7;
            }
                break;
            default:
                break;
            }
        }
            break;
        default:
            break;
        }
        if (iter.get_current()->get_time() >= 12)
            percentage -= 10;
        if (iter.get_current()->get_time() >= 24)
            percentage -= 10;
        if (iter.get_current()->get_time() >= 168)
            percentage -= 30;
        iter.next();
        tariff.count(percentage);
    }
    return tariff;
}

void Parking::xml_write_co(const QString filename)
{
    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    QXmlStreamWriter stream(&file);
    OwnIter<CarOwner> iter(car_o_l->front());
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement("CarOwners");
    for (int i = 0; i < car_o_l->get_size(); i++)
    {
        iter.get_current()->xml_write(stream, i);
        iter.next();
    }
    stream.writeEndElement();
    stream.writeEndDocument();
    file.close();
}
void Parking::xml_read_co(const QString filename)
{
    car_o_l->remove_all();
    car_l->remove_all();
    QFile file(filename);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        std::exception();
    }
    else
    {
        QXmlStreamReader stream;
        stream.setDevice(&file);
        stream.readNextStartElement();
        while (!stream.atEnd())
        {
            stream.readNextStartElement();
            CarOwner *curr = new CarOwner();
            if (stream.name() == "CarOwner")
            {
                curr = curr->xml_read(stream);
                add_elem(curr);
            }
        }
    }
    file.close();
}

QStringList Parking::get_co_list()const
{
    QStringList co_list;
    OwnIter<CarOwner> iter(car_o_l->front());
    for (int i = 0; i < car_o_l->get_size(); i++)
    {
        co_list += iter.get_current()->get_name() + " "
                + QString :: number(iter.get_current()->get_time())
                + " hours "
                + QString :: number(iter.get_current()->get_experience())
                + " years ";
        iter.next();
    }
    return  co_list;
}
QStringList Parking::get_car_list()const
{
    QStringList car_list;
    OwnIter<Car> iter(car_l->front());
    for (int i = 0; i < car_l->get_size(); i++)
    {
        QString item = iter.get_current()->get_car_mark() +
                " "+ iter.get_current()->get_car_num()+ " " ;
        switch (iter.get_current()->get_car_type())
        {
        case 1:
        {item += " Auto";}
            break;
        case 2:
        {item += " Truck";}
            break;
        case 3:
        {item += " Special transport";}
            break;
        }
        car_list += item;
        iter.next();
    }
    return  car_list;
}

QStringList Parking::get_only_smt_car(int fl) const
{
    if (fl == 1 or fl == 2 or fl == 3)
    {
        QStringList car_list;
        OwnIter<Car> iter(car_l->front());
        for (int i = 0; i < car_l->get_size(); i++)
        {
            if (iter.get_current()->get_car_type() == fl )
            {
                QString item = iter.get_current()->get_car_mark() +
                        " "+ iter.get_current()->get_car_num()+ " ";
                switch (fl)
                {
                case 1:
                {item += " Auto";}
                    break;
                case 2:
                {item += " Truck";}
                    break;
                case 3:
                {item += " Special transport";}
                    break;
                }
                car_list += item;
            }
            iter.next();
        }
        return car_list;
    }
    else
        std::exception();
}

QStringList Parking::get_only_smt_co(int fl) const
{
    if (fl == 1 or fl == 2 or fl == 3)
    {
        QStringList co_list;
        OwnIter<CarOwner> iter(car_o_l->front());
        for (int i = 0; i < car_l->get_size(); i++)
        {
            if (iter.get_current()->get_vehicle()->get_car_type() == fl )
            {
                QString item = iter.get_current()->get_name() + " "
                        + QString :: number(iter.get_current()->get_time())
                        + " hours "
                        + QString :: number(iter.get_current()->get_experience())
                        + " years ";
                co_list += item;
            }
            iter.next();
        }
        return co_list;
    }
    else
        std::exception();
}
bool Parking::check_file() const
{
    if (car_o_l && car_l)
    {
        return true;
    }
    else
        return false;
}
void Parking::fusion (Parking& to_fuse)
{
    car_o_l->append(*to_fuse.car_o_l);
    car_l->append(*to_fuse.car_l);
}

