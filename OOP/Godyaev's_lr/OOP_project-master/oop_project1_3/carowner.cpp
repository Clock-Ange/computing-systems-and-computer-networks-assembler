#include "carowner.h"
#include <math.h>
CarOwner::CarOwner()
{
    Car *tmp = new Auto();
    vehicle = tmp;
    name = "Default";
    experience = 0;
    time_hours = 1;
}
CarOwner::~CarOwner()
{
}
CarOwner::CarOwner(const CarOwner& tmp):
    name(tmp.get_name()),
    experience(tmp.get_experience()),
    time_hours(tmp.get_time())
{
    switch (tmp.get_vehicle()->get_car_type())
    {
    case 1:
        {
        Auto *curr = dynamic_cast<Auto*>(tmp.vehicle);
            set_vehicle(curr);
        }
        break;
    case 2:
        {
            Truck *curr = dynamic_cast<Truck*>(tmp.get_vehicle());
            set_vehicle(curr);
        }
        break;
    case 3:
        {
            SpecialTr *curr = dynamic_cast<SpecialTr*>(tmp.get_vehicle());
            set_vehicle(curr);
    }
        break;
    default:
        break;
    }
}
CarOwner::CarOwner(const Auto* tmp1,
                   const QString tmp2,
                   const int tmp3, const double tmp4):
    name(tmp2), experience(tmp3)
{
    set_time(tmp4);
    set_vehicle(tmp1);
}
CarOwner::CarOwner(const Truck* tmp1,
                   const QString tmp2,
                   const int tmp3, const double tmp4) :
    name(tmp2), experience(tmp3)
{
    set_time(tmp4);
    set_vehicle(tmp1);
}
CarOwner::CarOwner(const SpecialTr* tmp1,
                   const QString tmp2,
                   const int tmp3, const double tmp4) :
    name(tmp2), experience(tmp3)
{
    set_time(tmp4);
    set_vehicle(tmp1);
}
Car *CarOwner::get_vehicle() const
{
    return vehicle;
}
QString CarOwner::get_name() const
{
    return name;
}
int CarOwner::get_experience() const
{
    return experience;
}
int CarOwner::get_time() const
{
    return time_hours;
}
void CarOwner::set_vehicle(const Auto* tmp)
{
    if (tmp != nullptr)
    {
        Auto* item = new Auto(*tmp);
        vehicle = item;
    }
    else
    {
        Auto* item = new Auto();
        vehicle = item;
    }
}
void CarOwner::set_vehicle(const Truck* tmp)
{
    if (tmp != nullptr)
    {
        Truck* item = new Truck(*tmp);
        vehicle = item;
    }
    else
    {
        Truck* item = new Truck();
        vehicle = item;
    }
}
void CarOwner::set_vehicle(const SpecialTr* tmp)
{
    if (tmp != nullptr)
    {
        SpecialTr* item = new SpecialTr(*tmp);
        vehicle = item;
    }
    else
    {
        SpecialTr* item = new SpecialTr();
        vehicle = item;
    }
}
void CarOwner::set_name(const QString tmp)
{
    name = tmp;
}
void CarOwner::set_experience(const int tmp)
{
    if (tmp > 0 && tmp < 120)
        experience = tmp;
    else
        experience = 0;
}
void CarOwner::set_time(const double tmp)
{
    if (tmp > 0)
        time_hours = ceil(tmp);
    else
        time_hours = 1;
}
bool CarOwner::operator==(CarOwner& tmp)
{
    if (vehicle == tmp.get_vehicle() &&
        name == tmp.get_name() &&
        experience == tmp.get_experience())
        return true;
    else
        return false;
}
void CarOwner::xml_write(QXmlStreamWriter &stream, int i)
{
    stream.writeStartElement("CarOwner");
    stream.writeAttribute("number", QString :: number(i));

    stream.writeStartElement("Name");
    stream.writeCharacters(name);
    stream.writeEndElement();

    stream.writeStartElement("Experience");
    stream.writeCharacters(QString::number(experience));
    stream.writeEndElement();

    stream.writeStartElement("Vehicle");
    vehicle->xml_write(stream);
    stream.writeEndElement();

    stream.writeStartElement("Time");
    stream.writeCharacters(QString::number(time_hours));
    stream.writeEndElement();
    stream.writeEndElement();
}
CarOwner *CarOwner::xml_read(QXmlStreamReader& stream)
{
    CarOwner* curr = new CarOwner;
    stream.readNextStartElement();
    if (stream.name() == "Name")
        curr->set_name(stream.readElementText());
    stream.readNextStartElement();
    if (stream.name() == "Experience")
        curr->set_experience(
                    stream.readElementText().toInt());
    stream.readNextStartElement();
    if (stream.name() == "Vehicle")
    {
        stream.readNextStartElement();
        if(stream.name() == "Auto")
        {
            Auto* current = new Auto();
            curr->set_vehicle(current->xml_read(stream));
        }
        if(stream.name() == "Truck")
        {
            Truck* current = new Truck();
            curr->set_vehicle(current->xml_read(stream));
        }
        if(stream.name() == "SpecialTr")
        {
            SpecialTr* current = new SpecialTr();
            curr->set_vehicle(current->xml_read(stream));
        }
    }
    stream.readNextStartElement();

    stream.readNextStartElement();
    if (stream.name() == "Time")
        curr->set_time(stream.readElementText().toInt());
    stream.readNextStartElement();

    return curr;
}
