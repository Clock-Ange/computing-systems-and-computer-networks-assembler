#ifndef TRUCK_H
#define TRUCK_H
#include "car.h"

class Truck: public Car
{
public:
    Truck();
    Truck(const Truck&);
    Truck(const QString, const QString, const double);
    ~Truck() override;
    double get_tonnage() const;
    void set_tonnage(const double);
    int get_car_type() override;
    void xml_write(QXmlStreamWriter &) const override;
    Truck* xml_read(QXmlStreamReader&);
private:
    double tonnage;
};

#endif // TRUCK_H
