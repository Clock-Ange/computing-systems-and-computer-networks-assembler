#ifndef SPECIALTR_H
#define SPECIALTR_H
#include "car.h"
enum transport_type
{
    Motobyke, ATV, Scooter
};
class SpecialTr : public Car
{
public:
    SpecialTr();
    SpecialTr(const SpecialTr&);
    SpecialTr(const QString, const QString, const transport_type);
    ~SpecialTr() override;
    transport_type get_tr_type() const;
    void set_tr_type(const transport_type);
    int get_car_type() override;
    void xml_write(QXmlStreamWriter &) const override;
    SpecialTr* xml_read(QXmlStreamReader&);
private:
    transport_type type;
};

#endif // SPECIALTR_H
