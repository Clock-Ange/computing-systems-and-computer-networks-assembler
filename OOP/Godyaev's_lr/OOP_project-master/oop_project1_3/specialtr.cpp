#include <map>
#include "specialtr.h"
using namespace std;
SpecialTr::SpecialTr():Car()
{

}
SpecialTr::SpecialTr(const SpecialTr& tmp):Car(tmp)
{
    type = tmp.get_tr_type();
}
SpecialTr::SpecialTr(const QString tmp1,
    const QString tmp2, const transport_type tmp3):Car(tmp1, tmp2)
{
    type = tmp3;
}
SpecialTr::~SpecialTr(){}
inline void SpecialTr::set_tr_type(const transport_type tmp)
{
    type = tmp;
}
inline transport_type SpecialTr::get_tr_type() const
{
    return type;
}
int SpecialTr::get_car_type()
{
    return 3;
}
void SpecialTr::xml_write(QXmlStreamWriter& stream) const
{
    std::map<transport_type, QString> type_to_string;
    type_to_string.insert(pair<transport_type, QString>(Motobyke, "Motobyke"));
    type_to_string.insert(pair<transport_type, QString>(ATV, "ATV"));
    type_to_string.insert(pair<transport_type, QString>(Scooter, "Scooter"));
    stream.writeStartElement("SpecialTr");
        stream.writeStartElement("CarNumber");
        stream.writeCharacters(car_number);
        stream.writeEndElement();

        stream.writeStartElement("CarMark");
        stream.writeCharacters(car_mark);
        stream.writeEndElement();

        stream.writeStartElement("Type");
        stream.writeCharacters(type_to_string[type]);
        stream.writeEndElement();
    stream.writeEndElement();
}
SpecialTr *SpecialTr::xml_read(QXmlStreamReader& stream)
{
    std::map<QString, transport_type> string_to_type;
    string_to_type.insert(pair<QString, transport_type>("Motobyke", Motobyke));
    string_to_type.insert(pair<QString, transport_type>("ATV", ATV));
    string_to_type.insert(pair<QString, transport_type>("Scooter", Scooter));

    SpecialTr* curr = new SpecialTr;
    stream.readNextStartElement();
    if (stream.name() == "CarNumber")
        curr->set_car_num(stream.readElementText());
    stream.readNextStartElement();
    if (stream.name() == "CarMark")
        curr->set_car_mark(stream.readElementText());
    stream.readNextStartElement();
    if (stream.name() == "Tonnage")
        curr->set_tr_type(string_to_type[stream.readElementText()]);
    stream.readNextStartElement();
    return curr;
}
