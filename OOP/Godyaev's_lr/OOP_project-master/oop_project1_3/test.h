#ifndef TESTS_H
#define TESTS_H

#include <float.h>
#include <iostream>
#include <QRandomGenerator>
#include <QDateTime>
#include <QStringList>
#include <qtextstream.h>
#include <math.h>
#include "parking.h"
#define MIN_PARAM2_VALUE  -100.
#define MAX_PARAM2_VALUE +1000.
namespace Random
{

double realNumber(double from = -DBL_MAX/2, double to = DBL_MAX/2) {
    return (round(from + (to - from) * QRandomGenerator::global()->generateDouble())*100)/100;
}
int integer(int from = INT_MIN, int to = INT_MAX) {
    return QRandomGenerator::global()->bounded(from, to);
}
transport_type tr_type(int from = INT_MIN, int to = INT_MAX) {
    return transport_type(QRandomGenerator::global()->bounded(from, to));
};
const QString& itemFrom(const QStringList &list) {
    return list.at(QRandomGenerator::global()->bounded(list.length()));
}

QString car_num() {
    static QStringList cars_numbers = {"q123we", "a456sd", "z198xc", "p225oi",
                                       "m154po", "u654km", "o098mm", "t362vv",
                                       "n561dd", "k265bv", "o268gt", "p142qw"};
    return itemFrom(cars_numbers);
}
QString car_mark() {
    static QStringList car_marks = {"Ford", "Audi", "Renault", "Toyota",
                                    "Skoda", "Nissan", "Suzuki", "Opel"};
    return itemFrom(car_marks);
}
QString car_owners_names() {
    static QStringList names = {"Alex", "John", "Max", "Kim",
                                "Vasily", "Konstantin", "Vladislav", "Valentin"};
    return itemFrom(names);
}
CarOwner* rand_owner()
{
    switch (integer(1,3))
    {
    case 1:
    {
        Auto* curr = new Auto(car_mark(),car_num());
        CarOwner* current = new CarOwner(curr, car_owners_names(), integer(0, 50), realNumber(0, 48));
        return  current;
    }
    case 2:
    {
        Truck* curr = new Truck(car_mark(),car_num(), realNumber(0,30));
        CarOwner* current = new CarOwner(curr, car_owners_names(), integer(0, 50), realNumber(0, 48));
        return  current;
    }
    case 3:
    {
        SpecialTr* curr = new SpecialTr(car_mark(),car_num(), tr_type(1,3));
        CarOwner* current = new CarOwner(curr, car_owners_names(), integer(0, 50), realNumber(0, 48));
        return  current;
    }
    default:
        std::exception();
    }
}
Parking rand_parking()
{
    Parking current;
    int tmp = integer(1,10);
    for (int i = 0; i < tmp; i++)
        current.add_elem(rand_owner());
    return current;
}
inline std::string stdString(const QString &qstring) {
    return qstring.toLocal8Bit().toStdString();
}
}
#endif // TESTS_H
