#ifndef PARKING_H
#define PARKING_H
#include "list.h"
#include "car.h"
#include "carowner.h"
#include "rate.h"
const int free_spaces = 100;
class Parking
{
public:
    Parking();
    Parking(const Parking&);
    ~Parking();
    Rate count_sum();
    CarOwner* find_item(QString) const;
    void  add_elem(CarOwner*);
    void* delete_elem(const CarOwner*);
    QStringList get_co_list() const;
    QStringList get_car_list() const;
    QStringList get_only_smt_car(int) const; // 1 -auto; 2 - truck; 3 -special
    QStringList get_only_smt_co(int) const; // 1 -auto; 2 - truck; 3 -special
    void xml_write_co(const QString);
    void xml_read_co(const QString);
    bool check_file()const;
    void fusion(Parking &);
private:
    int number_of_free_spaces = free_spaces;
    List<CarOwner>* car_o_l;
    List<Car>* car_l;
};
#endif // PARKING_H
