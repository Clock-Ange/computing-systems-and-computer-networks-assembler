#include "truck.h"

Truck::Truck():Car()
{
    tonnage = 0;
}
Truck::Truck(const Truck& tmp):Car(tmp)
{
    tonnage = 0;
    set_tonnage(tmp.get_tonnage());
}
Truck::Truck(const QString tmp1,
    const QString tmp2, const double tmp3):Car(tmp1, tmp2)
{
    set_tonnage(tmp3);
}
Truck::~Truck(){}
inline void Truck::set_tonnage(const double tmp)
{
    if (tmp > 0 && tmp < 50)
        tonnage = tmp;
    else
        tonnage = 0;
}
inline double Truck::get_tonnage() const
{
    return tonnage;
}
int Truck::get_car_type()
{
    return 2;
}
void Truck::xml_write(QXmlStreamWriter &stream) const
{
    stream.writeStartElement("Truck");
        stream.writeStartElement("CarNumber");
        stream.writeCharacters(car_number);
        stream.writeEndElement();

        stream.writeStartElement("CarMark");
        stream.writeCharacters(car_mark);
        stream.writeEndElement();

        stream.writeStartElement("Tonnage");
        stream.writeCharacters(QString::number(tonnage));
        stream.writeEndElement();
    stream.writeEndElement();
}
Truck *Truck::xml_read(QXmlStreamReader& stream)
{
    Truck* curr = new Truck;
    stream.readNextStartElement();
    if (stream.name() == "CarNumber")
        curr->set_car_num(stream.readElementText());
    stream.readNextStartElement();
    if (stream.name() == "CarMark")
        curr->set_car_mark(stream.readElementText());
    stream.readNextStartElement();
    if (stream.name() == "Tonnage")
        curr->set_tonnage(stream.readElementText().toDouble());
    stream.readNextStartElement();
    return curr;
}
